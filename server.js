
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var movimientos_json = require('./json/movimientos_v2.json');
app.listen(port);

var body_parse = require('body-parser');
app.use(body_parse.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})
var request_json= require('request-json');

var url_mab_raiz="https://api.mlab.com/api/1/databases/sperez/collections";
var api_key="?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var url_clientes = url_mab_raiz+"/Clientes"+api_key;
var url_usuarios = url_mab_raiz+"/Usuarios"+api_key;

var cliente_mlab = request_json.createClient(url_clientes);
//var usuarios_mlab = request_json.createClient(url_usuarios);


console.log('La API REST corre sobre el puerto: ' + port);

app.get('/',function (request, response)
{
  response.sendFile(path.join(__dirname,'index.html'));
  console.log("request: "+request);
});

app.post('/',function (request, response)
{
  response.send('peticion POST cambiada');
});
app.put('/',function (request, response)
{
  response.send('peticion put');
});
app.delete('/',function (request, response)
{
  response.send('peticion DELETE');
});
app.get('/Clientes/:idCliente',function (request, response)
{
  //response.sendFile(path.join(__dirname,'index.html'));
  response.send("Aqui tiene al cliente: "+request.params.idCliente);
});
app.get('/v1/movimientos',function (request, response)
{
  //response.sendFile(path.join(__dirname,'index.html'));
  response.sendfile("json/movimientos_v1.json");
});
app.get('/v2/movimientos',function (request, response)
{
  //response.sendFile(path.join(__dirname,'index.html'));
  response.json(movimientos_json);
});
app.get('/v2/movimientos/:idMovimiento',function (request, response)
{
  //response.sendFile(path.join(__dirname,'index.html'));
  var index = request.params.idMovimiento;
  console.log("id: "+index);
  response.json(movimientos_json[index]);
});

app.get('/v2/movimientosQuery',function (request, response)
{
  //response.sendFile(path.join(__dirname,'index.html'));
  console.log("query: "+request.query);
  response.send("Recibimos query: "+request.query.nombre)
});
app.post('/v2/movimientos',function (request, response)
{
  var nuevo = request.body;
  nuevo.id= movimientos_json.length + 1;
  movimientos_json.push(nuevo);
  response.send("elemento registrado con el id: "+nuevo.id);
});
app.get('/Clientes',function(request,response)
{

    cliente_mlab.get('',function(error,response_message,body)
    {
         if(error)
         {
            console.log("error: "+body);
         }
         else
        {
            response.send(body);
         }
    });
});
app.post('/Clientes/addCliente',function(request,response)
{
  //var cliente_mlab = request_json.createClient(url_clientes);
    cliente_mlab.post('',request.body,function(error,response_message,body)
    {
         response.send(body);
    });
});
app.post('/login',function(request,response)
{
    response.set("Access-Control-Allow-Headers","Content-Type");
    var email = request.body.email;
    var password = request.body.password;

    var query = 'q={"email":"'+email+'","pasword":"'+password+'"}';
    console.log(query);

    var url_peticion = request_json.createClient(url_usuarios+"&"+query);
    console.log(url_usuarios+"&"+query);

    url_peticion.get('',function(error, response_message,body)
    {
        if(!error)
        {
            if(body.length == 1)
            {
                response.status(200).send("Usuario logueado");
            }
            else
            {
                response.status(400).send("Usuario NO existe");
            }
        }
    });
});
