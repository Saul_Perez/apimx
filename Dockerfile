#Image base
FROM node:latest

#directorio de la App en el contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#puerto que expongo
EXPOSE 3000

#comandos de ejecucion de la aplicacion
CMD ["npm","start"]
